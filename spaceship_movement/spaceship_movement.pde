Spaceship ship;
boolean[] movement;

void move() {
  float x = 0, y = 0;
  
  if ((!movement[0] && !movement[1]) || (movement[0] && movement[1])) {
    y = 0;
  } else if (movement[0]) {
    y = -1;
  } else {
    y = 1;
  }
  
  if ((!movement[2] && !movement[3]) || (movement[2] && movement[3])) {
    x = 0;
  } else if (movement[2]) {
    x = -1;
  } else {
    x = 1;
  }
  
  ship.modifyCentre(x, y);
}

void setup() {
  size(800, 800);
  background(255);
  
  ship = new Spaceship(40, 70, new float[]{width/2, height/2});
  
  movement = new boolean[]{false, false, false, false}; // [w, s, a, d]
}

void draw() {
  background(255);
  
  move();
  ship.drawShip();
}

void keyPressed() {
  switch (key) {
    case 'w':
    case 'W':
      movement[0] = true;
      break;
    case 's':
    case 'S':
      movement[1] = true;
      break;
    case 'a':
    case 'A':
      movement[2] = true;
      break;
    case 'd':
    case 'D':
      movement[3] = true;
      break;
  }
}

void keyReleased() {
  switch (key) {
    case 'w':
    case 'W':
      movement[0] = false;
      break;
    case 's':
    case 'S':
      movement[1] = false;
      break;
    case 'a':
    case 'A':
      movement[2] = false;
      break;
    case 'd':
    case 'D':
      movement[3] = false;
      break;
  }
}
