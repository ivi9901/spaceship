// TODO: Add speed of motion for each ship (in the constructor)
// TODO: Structure the class in such a way that you can create multiple ships with different rotation points and centres
// TODO: Change this junky centre to the mass centre of the triangle to make it look less weird

class Spaceship {
  private float[] centre;
  private float[][] baseCoordinates;
  private float[][] finalCoordinates;
  
  Spaceship(float tBase, float tHeight, float[] centre) {
    float halfBase = tBase/2;
    float halfHeight = tHeight/2;
    baseCoordinates = new float[][]{{0, halfHeight}, {-halfBase, -halfHeight}, {halfBase, -halfHeight}};
    this.centre = centre;
    
    this.finalCoordinates = new float[3][2];
  }
  
  public void modifyCentre(float x, float y) {
    this.centre[0] += x;
    this.centre[1] += y;
  }
  
  private void applyRotation(float angle) {    
    for (int i = 0; i < 3; i++) {
      this.finalCoordinates[i][0] = this.baseCoordinates[i][0]*cos(angle) - this.baseCoordinates[i][1]*sin(angle);
      this.finalCoordinates[i][1] = this.baseCoordinates[i][0]*sin(angle) + this.baseCoordinates[i][1]*cos(angle);
    }
  }  
  
  private void applyTranslation() {
    this.finalCoordinates[0][0] = this.finalCoordinates[0][0] + this.centre[0];
    this.finalCoordinates[0][1] = this.finalCoordinates[0][1] + this.centre[1];
    this.finalCoordinates[1][0] = this.finalCoordinates[1][0] + this.centre[0];
    this.finalCoordinates[1][1] = this.finalCoordinates[1][1] + this.centre[1];
    this.finalCoordinates[2][0] = this.finalCoordinates[2][0] + this.centre[0];
    this.finalCoordinates[2][1] = this.finalCoordinates[2][1] + this.centre[1];
  }
  
  private float getAngle() {
    // Calculating the slope here is not really feasible as it gives conflicting results. It's better to use atan2
    return atan2((mouseY - centre[1]), (mouseX - centre[0])) - PI/2;
  }
  
  public void drawShip() {
    applyRotation(getAngle());
    applyTranslation();
    triangle(finalCoordinates[0][0], finalCoordinates[0][1], finalCoordinates[1][0], finalCoordinates[1][1], finalCoordinates[2][0], finalCoordinates[2][1]);
    //circle(this.centre[0], this.centre[1], 5);
  }
}
