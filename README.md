Spaceship movement
==================

This small project aims to create a small "spaceship" (trhiangle) that you can move, emulating the old games.


Objectives
----------

The objective of this project is to create a small triangle in a 2D plain that can:
- Move using the WASD keys
- Aim where the mouse is aiming
